<?php
/**
 * Created by PhpStorm.
 * User: raphaelbussa
 * Date: 01/03/16
 * Time: 11:59
 */
include '../utils/config.php';
require '../libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->post('/login', function () use ($app) {
    $response = array();
    checkApiKey($app->request()->headers()->get('api-key'));
    global $tbl_user;
    $username = $app->request()->post('username');
    $avatar = $app->request()->post('avatar');
    $email = $app->request()->post('email');
    $user_id = getUserIdFromMail($email);
    if ($user_id != -1) {
        setTokenUser($user_id, $email);
        $response["success"] = true;
        $response["user"] = getUserJson($user_id, true);
        echoResponse(200, $response);
    } else {
        $connDb = connectionDb();
        $query = "INSERT INTO $tbl_user (username, avatar, email) VALUES ('$username', '$avatar', '$email')";
        $responseDb = mysqli_query($connDb, $query);
        if ($responseDb) {
            $user_id = getUserIdFromMail($email);
            setTokenUser($user_id, $email);
            $response["success"] = true;
            $response["user"] = getUserJson($user_id, true);
            echoResponse(200, $response);
        } else {
            echoError(1003);
        }
    }
});

$app->get('/attendance', function () use ($app) {
    $response = array();
    checkApiKey($app->request()->headers()->get('api-key'));
    $id_user = getUserIdFromToken($app->request()->headers()->get('token'));
    $response["success"] = true;
    $response["id_user"] = $id_user;
    $response["attendance"] = getUserAttendanceJson($id_user);
    echoResponse(200, $response);
});

$app->get('/attendance/:id', function ($id) use ($app) {
    $response = array();
    checkApiKey($app->request()->headers()->get('api-key'));
    $id_user = getUserIdFromToken($app->request()->headers()->get('token'));
    $response["success"] = true;
    $response["id_user"] = $id;
    $response["attendance"] = getUserAttendanceJson($id);
    echoResponse(200, $response);
});

$app->get('/self', function () use ($app) {
    $response = array();
    checkApiKey($app->request()->headers()->get('api-key'));
    $id_user = getUserIdFromToken($app->request()->headers()->get('token'));
    $response["success"] = true;
    $response["user"] = getUserJson($id_user, false);
    echoResponse(200, $response);
});

$app->get('/user/:id', function ($id) use ($app) {
    $response = array();
    checkApiKey($app->request()->headers()->get('api-key'));
    getUserIdFromToken($app->request()->headers()->get('token'));
    $response["success"] = true;
    $response["user"] = getUserJson($id, false);
    echoResponse(200, $response);
});

$app->get('/user/avatar/:id', function ($id) use ($app) {
    checkApiKey($app->request()->headers()->get('api-key'));
    getUserIdFromToken($app->request()->headers()->get('token'));
    $user = getUser($id);
    header('Content-Type: image/jpeg');
    readfile($user['avatar']);
    $app->stop();
});

$app->get('/users', function () use ($app) {
    global $tbl_user;
    checkApiKey($app->request()->headers()->get('api-key'));
    getUserIdFromToken($app->request()->headers()->get('token'));
    $connDb = connectionDb();
    $query = "SELECT * FROM $tbl_user";
    $responseDb = mysqli_query($connDb, $query);
    if (mysqli_num_rows($responseDb) > 0) {
        $response = array();
        $response["success"] = true;
        $dataset = array();
        while ($row = mysqli_fetch_array($responseDb)) {
            array_push($dataset, getUserJson($row['id'], false));
        }
        $response["users"] = $dataset;
        echoResponse(200, $response);
    } else {
        echoError(1003);
    }
});

$app->patch('/user', function () use ($app) {
    checkApiKey($app->request()->headers()->get('api-key'));
    global $tbl_user;
    $connDb = connectionDb();
    $id_user = getUserIdFromToken($app->request()->headers()->get('token'));
    $user_db = getUser($id_user);
    $github = $app->request()->patch('github');
    $whatsapp = $app->request()->patch('whatsapp');
    $telegram = $app->request()->patch('telegram');
    if ($github == null) {
        $github = $user_db['github'];
    }
    if ($whatsapp == null) {
        $whatsapp = $user_db['whatsapp'];
    }
    if ($telegram == null) {
        $telegram = $user_db['telegram'];
    }
    $query = "UPDATE $tbl_user SET github = '$github', whatsapp = '$whatsapp', telegram = '$telegram' WHERE id = '$id_user'";
    $responseDb = mysqli_query($connDb, $query);
    if ($responseDb) {
        $response["success"] = true;
        $response["user"] = getUserJson($id_user, false);
        echoResponse(200, $response);
    } else {
        echoError(1003);
    }
});

$app->get('/course', function () use ($app) {
    checkApiKey($app->request()->headers()->get('api-key'));
    getUserIdFromToken($app->request()->headers()->get('token'));
    global $tbl_course;
    $connDb = connectionDb();
    $query = "SELECT * FROM $tbl_course";
    $responseDb = mysqli_query($connDb, $query);
    if (mysqli_num_rows($responseDb) > 0) {
        $response = array();
        $response["success"] = true;
        $dataset = array();
        while ($row = mysqli_fetch_array($responseDb)) {
            $course = array();
            $course["id"] = (int) $row['id'];
            $course["name"] = utf8_encode($row['name']);
            $course["description"] = utf8_encode($row['description']);
            $course["start_date"] = (int) $row['start_date'];
            $course["end_date"] = (int) $row['end_date'];
            array_push($dataset, $course);
        }
        $response["course"] = $dataset;
        echoResponse(200, $response);
    } else {
        echoError(1007);
    }
});

$app->get('/course/:id', function ($id) use ($app) {
    checkApiKey($app->request()->headers()->get('api-key'));
    getUserIdFromToken($app->request()->headers()->get('token'));
    global $tbl_course;
    $connDb = connectionDb();
    $query = "SELECT * FROM $tbl_course WHERE id = '$id' LIMIT 1";
    $responseDb = mysqli_query($connDb, $query);
    if (mysqli_num_rows($responseDb) > 0) {
        $response = array();
        $response["success"] = true;
        $row = mysqli_fetch_array($responseDb);
        $course = array();
        $course["id"] = (int) $row['id'];
        $course["name"] = utf8_encode($row['name']);
        $course["description"] = utf8_encode($row['description']);
        $course["start_date"] = (int) $row['start_date'];
        $course["end_date"] = (int) $row['end_date'];
        $response["course"] = $course;
        echoResponse(200, $response);
    } else {
        echoError(1007);
    }
});

$app->post('/lesson', function () use ($app) {
    checkApiKey($app->request()->headers()->get('api-key'));
    $user_id = getUserIdFromToken($app->request()->headers()->get('token'));
    $user_db = getUser($user_id);
    if ($user_db['tutor'] == 0) {
        echoError(1012);
    }
    global $tbl_lesson;
    $title = $app->request()->post('title');
    $id_course = $app->request()->post('id_course');
    $descprition_short = $app->request()->post('descprition_short');
    $descprition_long = $app->request()->post('descprition_long');
    $link_drive = $app->request()->post('link_drive');
    $link_udacity = $app->request()->post('link_udacity');
    $lat = $app->request()->post('lat');
    $lng = $app->request()->post('lng');
    $start_date = $app->request()->post('start_date');
    $end_date = $app->request()->post('end_date');
    $address = getAddress($lat, $lng);
    $connDb = connectionDb();
    $query = "INSERT INTO $tbl_lesson (id_user, id_course, title, descprition_short, descprition_long, link_drive, lat, lng, start_date, end_date, link_udacity, address) VALUES ('$user_id', '$id_course', '$title', '$descprition_short', '$descprition_long', '$link_drive', '$lat', '$lng', '$start_date', '$end_date', '$link_udacity', '$address')";
    $responseDb = mysqli_query($connDb, $query);
    if ($responseDb) {
        $last_id = $connDb -> insert_id;
        $response["success"] = true;
        $response["lesson"] = getLessonJson($last_id);
        echoResponse(200, $response);
    } else {
        echoError(1003);
    }
});

$app->get('/lesson', function () use ($app) {
    checkApiKey($app->request()->headers()->get('api-key'));
    getUserIdFromToken($app->request()->headers()->get('token'));
    $course_id = $app->request()->get("id_course");
    global $tbl_lesson;
    $connDb = connectionDb();
    $query = "";
    if ($course_id == null) {
        $query = "SELECT * FROM $tbl_lesson";
    } else {
        $query = "SELECT * FROM $tbl_lesson WHERE id_course = '$course_id'";
    }
    $responseDb = mysqli_query($connDb, $query);
    if (mysqli_num_rows($responseDb) > 0) {
        $response = array();
        $response["success"] = true;
        $dataset = array();
        while ($row = mysqli_fetch_array($responseDb)) {
            array_push($dataset, getLessonJson($row['id']));
        }
        $response["lesson"] = $dataset;
        echoResponse(200, $response);
    } else {
        echoError(1007);
    }
});

$app->get('/lesson/:id', function ($id) use ($app) {
    checkApiKey($app->request()->headers()->get('api-key'));
    getUserIdFromToken($app->request()->headers()->get('token'));
    $response = array();
    $response["success"] = true;
    $response["lesson"] = getLessonJson($id);
    echoResponse(200, $response);
});

$app->get('/lesson/users/:id', function ($id) use ($app) {
    checkApiKey($app->request()->headers()->get('api-key'));
    getUserIdFromToken($app->request()->headers()->get('token'));
    global $tbl_user;
    $connDb = connectionDb();
    $query = "SELECT * FROM $tbl_user";
    $responseDb = mysqli_query($connDb, $query);
    if (mysqli_num_rows($responseDb) > 0) {
        $response = array();
        $response["success"] = true;
        $dataset = array();
        while ($row = mysqli_fetch_array($responseDb)) {
            $user = array();
            $user["present"] = checkIfUserIsPresent($row['id'], $id);
            $user["id"] = (int) $row['id'];
            $user["username"] = utf8_encode($row['username']);
            $user["avatar"] = utf8_encode($row['avatar']);
            array_push($dataset, $user);
        }
        $response["users"] = $dataset;
        echoResponse(200, $response);
    } else {
        echoError(1003);
    }
});

$app->post('/attendance', function () use ($app) {
    checkApiKey($app->request()->headers()->get('api-key'));
    $tutor_user_id = getUserIdFromToken($app->request()->headers()->get('token'));
    $user_db = getUser($tutor_user_id);
    if ($user_db['tutor'] == 0) {
        echoError(1012);
    }
    global $tbl_attendance;
    $id_user = $app->request()->post('id_user');
    $id_lession = $app->request()->post('id_lesson');
    $id_course = $app->request()->post('id_course');
    $valid = $app->request()->post('valid');
    $connDb = connectionDb();
    $query = "SELECT * FROM $tbl_attendance WHERE id_user = '$id_user' AND id_lession = '$id_lession' AND id_course = '$id_course' LIMIT 1";
    $responseDb = mysqli_query($connDb, $query);
    if (mysqli_num_rows($responseDb) > 0) {
        $query = "UPDATE $tbl_attendance SET valid = '$valid' WHERE id_user = '$id_user' AND id_lession = '$id_lession' AND id_course = '$id_course'";
        $responseDb = mysqli_query($connDb, $query);
        if ($responseDb) {
            $response = array();
            $response["success"] = true;
            echoResponse(200, $response);
        } else {
            echoError(1003);
        }
    } else {
        $query = "INSERT INTO $tbl_attendance (id_user, id_lession, id_course, valid) VALUES ('$id_user', '$id_lession','$id_course', '$valid')";
        $responseDb = mysqli_query($connDb, $query);
        if ($responseDb) {
            $response = array();
            $response["success"] = true;
            echoResponse(200, $response);
        } else {
            echoError(1003);
        }
    }
});


$app->get('/hello/:name', function ($name) {
    echo "Hello, $name";
});

function setTokenUser($id, $email) {
    global $tbl_user;
    $connDb = connectionDb();
    $token = md5($email . time());
    $query = "UPDATE $tbl_user SET token = '$token' WHERE id = '$id'";
    $responseDb = mysqli_query($connDb, $query);
    if (!$responseDb) {
        echoError(1003);
    }
}

function getUserJson($id, $token) {
    $user_db = getUser($id);
    $user = array();
    $user["id"] = (int) $user_db['id'];
    $user["name"] = utf8_encode($user_db['username']);
    $user["avatar"] = utf8_encode($user_db['avatar']);
    $user["github"] = utf8_encode($user_db['github']);
    $user["email"] = utf8_encode($user_db['email']);
    $user["tutor"] = (int) $user_db['tutor'];
    if ($token) {
        $user["token"] = utf8_encode($user_db['token']);
    }
    $user["whatsapp"] = utf8_encode($user_db['whatsapp']);
    $user["telegram"] = utf8_encode($user_db['telegram']);
    $user["attendance"] = getUserAttendanceJson($id);
    return $user;
}

function checkIfUserIsPresent($user_id, $lesson_id) {
    global $tbl_attendance;
    $connDb = connectionDb();
    $query = "SELECT * FROM $tbl_attendance WHERE id_user = '$user_id' AND id_lession = '$lesson_id' LIMIT 1";
    $responseDbCourse = mysqli_query($connDb, $query);
    $row = mysqli_fetch_array($responseDbCourse);
    if ($row['valid'] == 1) {
        return true;
    } else {
        return false;
    }
}

function getUserAttendanceJson($id) {
    global $tbl_attendance;
    global $tbl_course;
    global $tbl_lesson;
    $connDb = connectionDb();
    $query = "SELECT * FROM $tbl_course";
    $responseDbCourse = mysqli_query($connDb, $query);
    $response = array();
    if (mysqli_num_rows($responseDbCourse) > 0) {
        while ($row = mysqli_fetch_array($responseDbCourse)) {
            $id_course = $row['id'];
            $attendance = array();
            $attendance["id_course"] = $id_course;
            $query = "SELECT * FROM $tbl_lesson WHERE id_course = '$id_course'";
            $responseDbLesson = mysqli_query($connDb, $query);
            $attendance["num_lesson"] = mysqli_num_rows($responseDbLesson);
            $query = "SELECT * FROM $tbl_attendance WHERE id_user = '$id' AND id_course = '$id_course' AND valid = 1";
            $responseDbAttendance = mysqli_query($connDb, $query);
            $attendance["num_lesson_user"] = mysqli_num_rows($responseDbAttendance);
            array_push($response, $attendance);
        }
        return $response;
    } else {
        return null;
    }
}


$app->post('/drop/user', function () use ($app)  {
    global $tbl_user;
    $response = array();
    $username = $app->request()->post('username');
    $password = $app->request()->post('password');
    if ($username == "developer" && $password == "glad1@tore2015") {
        $connDb = connectionDb();
        $query = "TRUNCATE TABLE $tbl_user";
        $responseDb = mysqli_query($connDb, $query);
        if ($responseDb) {
            $response["success"] = true;
            echoResponse(200, $response);
        } else {
            $response["success"] = false;
            $response["message"] = "error mysql";
            echoResponse(200, $response);
        }
    } else {
        $response["success"] = false;
        $response["message"] = "not authorized";
        echoResponse(200, $response);
    }
});

$app->post('/drop/attendance', function () use ($app)  {
    global $tbl_attendance;
    $response = array();
    $username = $app->request()->post('username');
    $password = $app->request()->post('password');
    if ($username == "developer" && $password == "glad1@tore2015") {
        $connDb = connectionDb();
        $query = "TRUNCATE TABLE $tbl_attendance";
        $responseDb = mysqli_query($connDb, $query);
        if ($responseDb) {
            $response["success"] = true;
            echoResponse(200, $response);
        } else {
            $response["success"] = false;
            $response["message"] = "error mysql";
            echoResponse(200, $response);
        }
    } else {
        $response["success"] = false;
        $response["message"] = "not authorized";
        echoResponse(200, $response);
    }
});

$app->post('/drop/lesson', function () use ($app)  {
    global $tbl_lesson;
    $response = array();
    $username = $app->request()->post('username');
    $password = $app->request()->post('password');
    if ($username == "developer" && $password == "glad1@tore2015") {
        $connDb = connectionDb();
        $query = "TRUNCATE TABLE $tbl_lesson";
        $responseDb = mysqli_query($connDb, $query);
        if ($responseDb) {
            $response["success"] = true;
            echoResponse(200, $response);
        } else {
            $response["success"] = false;
            $response["message"] = "error mysql";
            echoResponse(200, $response);
        }
    } else {
        $response["success"] = false;
        $response["message"] = "not authorized";
        echoResponse(200, $response);
    }
});

$app->post('/drop/course', function () use ($app)  {
    global $tbl_course;
    $response = array();
    $username = $app->request()->post('username');
    $password = $app->request()->post('password');
    if ($username == "developer" && $password == "glad1@tore2015") {
        $connDb = connectionDb();
        $query = "TRUNCATE TABLE $tbl_course";
        $responseDb = mysqli_query($connDb, $query);
        if ($responseDb) {
            $response["success"] = true;
            echoResponse(200, $response);
        } else {
            $response["success"] = false;
            $response["message"] = "error mysql";
            echoResponse(200, $response);
        }
    } else {
        $response["success"] = false;
        $response["message"] = "not authorized";
        echoResponse(200, $response);
    }
});

function getUser($id) {
    global $tbl_user;
    $connDb = connectionDb();
    $query = "SELECT * FROM $tbl_user WHERE id = '$id' LIMIT 1";
    $responseDb = mysqli_query($connDb, $query);
    if (mysqli_num_rows($responseDb) > 0) {
        return mysqli_fetch_array($responseDb);
    } else {
        echoError(1005);
        return null;
    }
}

function getLessonJson($id) {
    $lesson_db = getLesson($id);
    $lesson = array();
    $lesson["id"] = (int) $lesson_db['id'];
    $lesson["id_user"] = (int) $lesson_db['id_user'];
    $lesson["id_course"] = (int) $lesson_db['id_course'];
    $lesson["title"] = utf8_encode($lesson_db['title']);
    $lesson["descprition_short"] = utf8_encode($lesson_db['descprition_short']);
    $lesson["descprition_long"] = utf8_encode($lesson_db['descprition_long']);
    $lesson["link_drive"] = utf8_encode($lesson_db['link_drive']);
    $lesson["link_udacity"] = utf8_encode($lesson_db['link_udacity']);
    $lesson["address"] = utf8_encode($lesson_db['address']);
    $lesson["lat"] = (double) $lesson_db['lat'];
    $lesson["lng"] = (double) $lesson_db['lng'];
    $lesson["start_date"] = (int) $lesson_db['start_date'];
    $lesson["end_date"] = (int) $lesson_db['end_date'];
    return $lesson;
}

function getLesson($id) {
    global $tbl_lesson;
    $connDb = connectionDb();
    $query = "SELECT * FROM $tbl_lesson WHERE id = '$id' LIMIT 1";
    $responseDb = mysqli_query($connDb, $query);
    if (mysqli_num_rows($responseDb) > 0) {
        return mysqli_fetch_array($responseDb);
    } else {
        echoError(1005);
        return null;
    }
}

function getUserIdFromMail($email) {
    global $tbl_user;
    $connDb = connectionDb();
    $query = "SELECT * FROM $tbl_user WHERE email = '$email' LIMIT 1";
    $responseDb = mysqli_query($connDb, $query);
    if (mysqli_num_rows($responseDb) > 0) {
        $row = mysqli_fetch_array($responseDb);
        return $row['id'];
    } else {
        return -1;
    }
}

function getUserIdFromToken($token) {
    global $tbl_user;
    $connDb = connectionDb();
    $query = "SELECT * FROM $tbl_user WHERE token = '$token' LIMIT 1";
    $responseDb = mysqli_query($connDb, $query);
    if (mysqli_num_rows($responseDb) > 0) {
        $row = mysqli_fetch_array($responseDb);
        return $row['id'];
    } else {
        echoError(1005);
        return -1;
    }
}

function connectionDb() {
    global $host;
    global $db_username;
    global $db_psw;
    global $db_name;
    $connDb = mysqli_connect($host, $db_username, $db_psw, $db_name);
    if ($connDb) {
        return mysqli_connect($host, $db_username, $db_psw, $db_name);
    } else {
        echoError(1001);
        return null;
    }
}

function checkApiKey($key)
{
    global $api_key;
    if ($api_key != $key) {
        echoError(2000);
    }
}

function getAddress($lat, $lng) {
    $url = "https://maps.googleapis.com/maps/api/geocode/xml?latlng=$lat,$lng";
    $response = file_get_contents($url);
    $xml = new SimpleXMLElement($response);
    return $xml->result->formatted_address;
}

function echoError($error_code) {
    $app = \Slim\Slim::getInstance();
    $app->status(200);
    $app->contentType('application/json');
    $response = array();
    $response["success"] = false;
    $response["code"] = $error_code;
    switch ($error_code) {
        case 1000:
            $response["message"] = "some required values are null";
            break;
        case 1001:
            $response["message"] = "database connection error";
            break;
        case 1002:
            $response["message"] = "user already registered";
            break;
        case 1003:
            $response["message"] = "error in the insert in the database";
            break;
        case 1004:
            $response["message"] = "username and password not match";
            break;
        case 1005:
            $response["message"] = "invalid token";
            break;
        case 1006:
            $response["message"] = "wrong email format";
            break;
        case 1007:
            $response["message"] = "point not found";
            break;
        case 1008:
            $response["message"] = "no point for this query";
            break;
        case 1009:
            $response["message"] = "comment not found";
            break;
        case 1010:
            $response["message"] = "vote yet expressed";
            break;
        case 1011:
            $response["message"] = "request compiled bad";
            break;
        case 1012:
            $response["message"] = "user not authorized";
            break;
        case 2000:
            $response["message"] = "wrong api key";
            break;
        default:
            $response["message"] = "no error message find :(";
            break;
    }
    echo json_encode($response);
    $app->stop();
}

function echoResponse($status_code, $response)
{
    $app = \Slim\Slim::getInstance();
    $app->status($status_code);
    $app->contentType('application/json');
    echo json_encode($response);
    $app->stop();
}

$app->run();
