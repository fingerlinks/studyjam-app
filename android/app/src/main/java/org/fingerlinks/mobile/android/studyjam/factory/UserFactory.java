package org.fingerlinks.mobile.android.studyjam.factory;

import android.content.Context;
import android.text.TextUtils;

import com.koushikdutta.async.future.FutureCallback;

import org.fingerlinks.mobile.android.studyjam.SharedPreferenceAdapter;
import org.fingerlinks.mobile.android.studyjam.http.HttpRequestClient;
import org.fingerlinks.mobile.android.studyjam.http.response.login.LoginResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.login.bean.User;
import org.fingerlinks.mobile.android.studyjam.model.UserModel;

import io.realm.Realm;

/**
 * Created by raphaelbussa on 02/03/16.
 */
public class UserFactory {

    private static UserFactory instance;
    private Context context;

    public static UserFactory with(Context context) {
        if (instance == null) {
            instance = new UserFactory();
        }
        instance.init(context);
        return instance;
    }

    private void init(Context context) {
        this.context = context;
    }

    public boolean isLogged() {
        return (!TextUtils.isEmpty(SharedPreferenceAdapter.with(context).getToken()));
    }

    public UserModel getUser() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(UserModel.class).equalTo("token", SharedPreferenceAdapter.with(context).getToken()).findFirst();
    }

    public void login(User user) {
        SharedPreferenceAdapter.with(context).setToken(user.getToken());
        UserModel userModel = new UserModel();
        userModel.setId(user.getId());
        userModel.setName(user.getName());
        userModel.setAvatar(user.getAvatar());
        userModel.setGithub(user.getGithub());
        userModel.setEmail(user.getEmail());
        userModel.setTutor(user.getTutor());
        userModel.setToken(user.getToken());
        userModel.setWhatsapp(user.getWhatsapp());
        userModel.setTelegram(user.getTelegram());
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(userModel);
        realm.commitTransaction();
    }

    public void updateUser(User user) {
        UserModel userModel = new UserModel();
        userModel.setId(user.getId());
        userModel.setName(user.getName());
        userModel.setAvatar(user.getAvatar());
        userModel.setGithub(user.getGithub());
        userModel.setEmail(user.getEmail());
        userModel.setTutor(user.getTutor());
        userModel.setToken(SharedPreferenceAdapter.with(context).getToken());
        userModel.setWhatsapp(user.getWhatsapp());
        userModel.setTelegram(user.getTelegram());
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(userModel);
        realm.commitTransaction();
    }

    public void getUserUpdated(final CallBack callBack) {
        HttpRequestClient.self(context, new FutureCallback<LoginResponse>() {
            @Override
            public void onCompleted(Exception e, LoginResponse result) {
                if (e == null) {
                    if (result.isSuccess()) {
                        updateUser(result.getUser());
                    }
                }
                callBack.onFinish(getUser());
            }
        });
    }

    public interface CallBack {
        void onFinish(UserModel userModel);
    }

}
