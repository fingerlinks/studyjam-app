package org.fingerlinks.mobile.android.studyjam.http.bean;

import java.io.Serializable;

/**
 * Created by raphaelbussa on 04/03/16.
 */
public class UpdateUser implements Serializable {

    private String github;
    private String telegram;
    private String whatsapp;

    public String getGithub() {
        return github;
    }

    public void setGithub(String github) {
        this.github = github;
    }

    public String getTelegram() {
        return telegram;
    }

    public void setTelegram(String telegram) {
        this.telegram = telegram;
    }

    public String getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(String whatsapp) {
        this.whatsapp = whatsapp;
    }
}
