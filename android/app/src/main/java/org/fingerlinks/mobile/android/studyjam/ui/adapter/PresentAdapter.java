package org.fingerlinks.mobile.android.studyjam.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.koushikdutta.async.future.FutureCallback;

import org.fingerlinks.mobile.android.studyjam.R;
import org.fingerlinks.mobile.android.studyjam.http.HttpRequestClient;
import org.fingerlinks.mobile.android.studyjam.http.response.BaseResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.presence.bean.Presence;
import org.fingerlinks.mobile.android.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by raphaelbussa on 02/03/16.
 */
public class PresentAdapter extends RecyclerView.Adapter<PresentAdapter.MainViewHolder> {

    private List<Presence> lessonModelList;
    private Context context;
    private int idLesson;

    public PresentAdapter(Context context, int idLesson) {
        this.context = context;
        this.idLesson = idLesson;
        this.lessonModelList = new ArrayList<>();
    }

    public void setLessonModelList(List<Presence> lessonModelList) {
        this.lessonModelList.clear();
        this.lessonModelList = lessonModelList;
        this.notifyDataSetChanged();
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_users, parent, false);
        return new MainViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MainViewHolder holder, int position) {
        final Presence presence = lessonModelList.get(position);
        holder.checkBox.setChecked(presence.isPresent());
        holder.name.setText(Utils.encodeString(presence.getUsername()));
        Glide.with(context)
                .load(presence.getAvatar())
                .asBitmap()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.avatar);
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.checkBox.setEnabled(false);
                HttpRequestClient.setAttendance(context,
                        holder.checkBox.isChecked(),
                        presence.getId(),
                        idLesson,
                        new FutureCallback<BaseResponse>() {
                            @Override
                            public void onCompleted(Exception e, BaseResponse result) {
                                holder.checkBox.setEnabled(true);
                                if (e != null) {
                                    holder.checkBox.setChecked(!holder.checkBox.isChecked());
                                    return;
                                }
                                if (!result.isSuccess()) {
                                    holder.checkBox.setChecked(!holder.checkBox.isChecked());
                                }
                            }
                        });
            }
        });
    }

    @Override
    public int getItemCount() {
        return lessonModelList.size();
    }

    public class MainViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView avatar;
        private TextView name;
        private CheckBox checkBox;

        public MainViewHolder(View itemView) {
            super(itemView);
            avatar = (CircleImageView) itemView.findViewById(R.id.avatar);
            name = (TextView) itemView.findViewById(R.id.name);
            checkBox = (CheckBox) itemView.findViewById(R.id.present);
        }
    }
}

