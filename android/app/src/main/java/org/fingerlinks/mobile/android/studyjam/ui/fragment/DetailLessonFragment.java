package org.fingerlinks.mobile.android.studyjam.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.koushikdutta.async.future.FutureCallback;
import com.mikepenz.iconics.view.IconicsImageView;
import com.nineoldandroids.view.ViewHelper;

import org.fingerlinks.mobile.android.navigator.Navigator;
import org.fingerlinks.mobile.android.studyjam.Constants;
import org.fingerlinks.mobile.android.studyjam.R;
import org.fingerlinks.mobile.android.studyjam.factory.UserFactory;
import org.fingerlinks.mobile.android.studyjam.http.HttpRequestClient;
import org.fingerlinks.mobile.android.studyjam.http.response.login.LoginResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.login.bean.User;
import org.fingerlinks.mobile.android.studyjam.model.LessonModel;
import org.fingerlinks.mobile.android.studyjam.model.UserModel;
import org.fingerlinks.mobile.android.utils.SocialIntent;
import org.fingerlinks.mobile.android.utils.Utils;
import org.fingerlinks.mobile.android.utils.fragment.BaseFragment;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

/**
 * Created by raphaelbussa on 02/03/16.
 */
public class DetailLessonFragment extends BaseFragment implements ObservableScrollViewCallbacks {

    private int id;
    private LessonModel lessonModel;
    private User user;

    private RelativeLayout header;
    private ImageView map;
    private FloatingActionButton fab;
    private CircleImageView avatar;
    private TextView name;
    private TextView email;
    private IconicsImageView whatsapp;
    private IconicsImageView telegram;
    private IconicsImageView hangout;
    private IconicsImageView github;
    private ObservableScrollView scrollView;
    private TextView description;
    private TextView drive;
    private TextView udacity;

    private int dp_200;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        id = getArguments().getInt("ID");
        dp_200 = getActivity().getResources().getDimensionPixelSize(R.dimen.dp_200);
        initView(view);
        prepareQuery();
    }

    private void setUser() {
        if (user != null) {
            Glide.with(getContext())
                    .load(user.getAvatar())
                    .asBitmap()
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(avatar);
            name.setText(Html.fromHtml(user.getName()));
            email.setText(user.getEmail());
            whatsapp.setTag(user.getWhatsapp());
            whatsapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String value = (String) view.getTag();
                    SocialIntent.whatsapp(getActivity(), value);
                }
            });
            telegram.setTag(user.getTelegram());
            telegram.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String value = (String) view.getTag();
                    SocialIntent.telegram(getActivity(), value);
                }
            });
            github.setTag(user.getGithub());
            github.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String value = (String) view.getTag();
                    SocialIntent.github(getActivity(), value);
                }
            });
            hangout.setTag(user.getEmail());
            hangout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String value = (String) view.getTag();
                    SocialIntent.hangout(getActivity(), value);
                }
            });

            checkIfValueIsPresent(user.getGithub(), github);
            checkIfValueIsPresent(user.getWhatsapp(), whatsapp);
            checkIfValueIsPresent(user.getTelegram(), telegram);
            checkIfValueIsPresent(user.getEmail(), hangout);
        }
    }

    private void checkIfValueIsPresent(String value, IconicsImageView iconicsButton) {
        if (TextUtils.isEmpty(value)) {
            iconicsButton.setVisibility(View.GONE);
        } else {
            iconicsButton.setVisibility(View.VISIBLE);
        }
    }

    private void setLesson() {
        if (lessonModel != null) {
            setSubtitle(lessonModel.getTitle());
            Glide.with(getActivity())
                    .load(Utils.getStaticMapUrl(lessonModel.getLat(), lessonModel.getLng(), 400, 200))
                    .asBitmap()
                    .into(map);
            description.setText(Html.fromHtml(lessonModel.getDescprition_long()));
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri gmmIntentUri = Uri.parse("google.navigation:q=" + lessonModel.getLat() + "," + lessonModel.getLng());
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);
                }
            });
            drive.setTag(lessonModel.getLink_drive());
            udacity.setTag(lessonModel.getLink_udacity());
            drive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String value = (String) v.getTag();
                    if (!TextUtils.isEmpty(value)) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(value));
                        getActivity().startActivity(intent);
                    }
                }
            });
            udacity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String value = (String) v.getTag();
                    if (!TextUtils.isEmpty(value)) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(value));
                        getActivity().startActivity(intent);
                    }
                }
            });
        }
    }

    private void prepareQuery() {
        Realm realm = Realm.getDefaultInstance();
        lessonModel = realm.where(LessonModel.class).equalTo("id", id).findFirst();
        setLesson();
        HttpRequestClient.user(getActivity(), lessonModel.getId_user(), new FutureCallback<LoginResponse>() {
            @Override
            public void onCompleted(Exception e, LoginResponse result) {
                if (e != null) return;
                if (result.isSuccess()) user = result.getUser();
                setUser();
            }
        });
    }

    private void initView(View view) {
        header = (RelativeLayout) view.findViewById(R.id.header);
        map = (ImageView) view.findViewById(R.id.map);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        avatar = (CircleImageView) view.findViewById(R.id.avatar);
        name = (TextView) view.findViewById(R.id.name);
        email = (TextView) view.findViewById(R.id.email);
        whatsapp = (IconicsImageView) view.findViewById(R.id.whatsapp);
        telegram = (IconicsImageView) view.findViewById(R.id.telegram);
        hangout = (IconicsImageView) view.findViewById(R.id.hangouts);
        github = (IconicsImageView) view.findViewById(R.id.github);
        scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        description = (TextView) view.findViewById(R.id.description);
        drive = (TextView) view.findViewById(R.id.drive);
        udacity = (TextView) view.findViewById(R.id.udacity);
        scrollView.setScrollViewCallbacks(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        setNavigationIcon(R.drawable.md_nav_back);
        setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigator.with(getActivity())
                        .utils()
                        .goBackToSpecificPoint(Constants.FRAGMENT_COURSE);
            }
        });
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        float value = ScrollUtils.getFloat(scrollY, 0, dp_200);
        ViewHelper.setTranslationY(header, -value);
        float alpha = 1 - Math.min(1, (float) scrollY / dp_200);
        ViewHelper.setScaleY(fab, alpha);
        ViewHelper.setScaleX(fab, alpha);
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_detail_lesson;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) menu.clear();
        UserModel userModel = UserFactory.with(getActivity()).getUser();
        if (userModel.getTutor() == 1) {
            inflater.inflate(R.menu.lesson_tutor_menu, menu);
        } else {
            inflater.inflate(R.menu.lesson_menu, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share:
                Intent i = new Intent(android.content.Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(android.content.Intent.EXTRA_TEXT, lessonModel.getTitle() + "\n\n" + lessonModel.getDescprition_short());
                getActivity().startActivity(Intent.createChooser(i, getResources().getText(R.string.share)));
                break;
            case R.id.presence:
                Bundle bundle = new Bundle();
                bundle.putInt("ID", lessonModel.getId());
                Navigator.with(getActivity())
                        .build()
                        .goTo(Fragment.instantiate(getActivity(), PresenceFragment.class.getName()), bundle, R.id.container)
                        .animation()
                        .tag(Constants.FRAGMENT_PRESENCE)
                        .addToBackStack()
                        .add()
                        .commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
