package org.fingerlinks.mobile.android.studyjam.http.response;

import java.io.Serializable;

/**
 * Created by raphaelbussa on 01/03/16.
 */
public class BaseResponse implements Serializable {

    private boolean success;
    private int code;
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
