package org.fingerlinks.mobile.android.studyjam.factory;

import org.fingerlinks.mobile.android.studyjam.http.response.course.bean.Course;
import org.fingerlinks.mobile.android.studyjam.http.response.lesson.bean.Lesson;
import org.fingerlinks.mobile.android.studyjam.model.CourseModel;
import org.fingerlinks.mobile.android.studyjam.model.LessonModel;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by raphaelbussa on 02/03/16.
 */
public class ConvertFacory {

    public static void saveCourse(ArrayList<Course> courseArrayList) {
        List<CourseModel> courseModelList = new ArrayList<>();
        for (Course course : courseArrayList) {
            courseModelList.add(courseModel(course));
        }
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(courseModelList);
        realm.commitTransaction();
    }

    public static void saveLesson(ArrayList<Lesson> lessonArrayList) {
        List<LessonModel> lessonModelList = new ArrayList<>();
        for (Lesson lesson : lessonArrayList) {
            lessonModelList.add(lessonModel(lesson));
        }
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(lessonModelList);
        realm.commitTransaction();
    }

    private static CourseModel courseModel(Course course) {
        CourseModel courseModel = new CourseModel();
        courseModel.setId(course.getId());
        courseModel.setName(course.getName());
        courseModel.setDescription(course.getDescription());
        courseModel.setStart_date(course.getStart_date());
        courseModel.setEnd_date(course.getEnd_date());
        return courseModel;
    }

    private static LessonModel lessonModel(Lesson lesson) {
        LessonModel lessonModel = new LessonModel();
        lessonModel.setId(lesson.getId());
        lessonModel.setId_user(lesson.getId_user());
        lessonModel.setId_course(lesson.getId_course());
        lessonModel.setTitle(lesson.getTitle());
        lessonModel.setDescprition_short(lesson.getDescprition_short());
        lessonModel.setDescprition_long(lesson.getDescprition_long());
        lessonModel.setLink_drive(lesson.getLink_drive());
        lessonModel.setLink_udacity(lesson.getLink_udacity());
        lessonModel.setAddress(lesson.getAddress());
        lessonModel.setLat(lesson.getLat());
        lessonModel.setLng(lesson.getLng());
        lessonModel.setStart_date(lesson.getStart_date());
        lessonModel.setEnd_date(lesson.getEnd_date());
        return lessonModel;
    }

}
