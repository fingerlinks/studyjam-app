package org.fingerlinks.mobile.android.studyjam.http.response.login.bean;

import java.io.Serializable;

/**
 * Created by raphaelbussa on 02/03/16.
 */
public class Attendance implements Serializable {

    private int id_course;
    private int num_lesson;
    private int num_lesson_user;

    public int getId_course() {
        return id_course;
    }

    public void setId_course(int id_course) {
        this.id_course = id_course;
    }

    public int getNum_lesson() {
        return num_lesson;
    }

    public void setNum_lesson(int num_lesson) {
        this.num_lesson = num_lesson;
    }

    public int getNum_lesson_user() {
        return num_lesson_user;
    }

    public void setNum_lesson_user(int num_lesson_user) {
        this.num_lesson_user = num_lesson_user;
    }

}
