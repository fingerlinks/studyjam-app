package org.fingerlinks.mobile.android.utils;

import android.content.Context;
import android.content.DialogInterface;

import com.afollestad.materialdialogs.MaterialDialog;

import org.fingerlinks.mobile.android.studyjam.R;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by raphaelbussa on 02/03/16.
 */
public class Utils {

    public static String removeNull(String value) {
        if (value == null) {
            return "";
        } else {
            return value;
        }
    }

    public static String formatData(long value) {
        String format = "EEE, d MMM yyyy HH:mm";
        Date date = new Date(value * 1000);
        DateFormat dateFormat = new SimpleDateFormat(format, Locale.ITALY);
        return dateFormat.format(date);
    }

    public static void genericError(Context context, DialogInterface.OnDismissListener dismissListener) {
        genericError(context, context.getString(R.string.generic_error), dismissListener);
    }

    public static void genericError(Context context, String text, DialogInterface.OnDismissListener dismissListener) {
        new MaterialDialog.Builder(context)
                .content(text)
                .dismissListener(dismissListener)
                .positiveText(R.string.ok)
                .show();
    }

    public static String encodeString(String value) {
        try {
            return new String(value.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    public static String getStaticMapUrl(double lat, double lng, int horizontal_value, int vertical_value) {
        return "http://maps.google.com/maps/api/staticmap?center=" + lat +
                "," + lng +
                "&markers=icon " + lat + "," + lng +
                "&zoom=14" +
                "&size=" + horizontal_value + "x" + vertical_value + "&sensor=false";
    }

}
