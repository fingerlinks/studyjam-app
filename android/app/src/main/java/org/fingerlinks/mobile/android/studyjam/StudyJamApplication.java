package org.fingerlinks.mobile.android.studyjam;

import android.app.Application;

import org.fingerlinks.mobile.android.studyjam.model.CourseModel;
import org.fingerlinks.mobile.android.studyjam.model.LessonModel;
import org.fingerlinks.mobile.android.studyjam.model.UserModel;

import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.annotations.RealmModule;

/**
 * Created by raphaelbussa on 01/03/16.
 */
public class StudyJamApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        RealmConfiguration config = new RealmConfiguration.Builder(getApplicationContext())
                .name(getResources().getString(R.string.database_conf_name))
                .schemaVersion(getResources().getInteger(R.integer.database_conf_version))
                .setModules(new DbModule())
                .deleteRealmIfMigrationNeeded()
                .migration(new RealmMigration() {
                    @Override
                    public void migrate(DynamicRealm dynamicRealm, long l, long l1) {

                    }
                })
                .build();
        Realm.setDefaultConfiguration(config);
    }

    @RealmModule(classes = {
            UserModel.class, CourseModel.class, LessonModel.class
    })
    class DbModule {
    }


}
