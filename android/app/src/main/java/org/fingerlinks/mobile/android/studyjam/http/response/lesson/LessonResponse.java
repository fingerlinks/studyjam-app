package org.fingerlinks.mobile.android.studyjam.http.response.lesson;

import org.fingerlinks.mobile.android.studyjam.http.response.BaseResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.lesson.bean.Lesson;

import java.util.ArrayList;

/**
 * Created by raphaelbussa on 01/03/16.
 */
public class LessonResponse extends BaseResponse {

    private ArrayList<Lesson> lesson;

    public ArrayList<Lesson> getLesson() {
        return lesson;
    }

    public void setLesson(ArrayList<Lesson> lesson) {
        this.lesson = lesson;
    }
}
