package org.fingerlinks.mobile.android.studyjam;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by raphaelbussa on 02/03/16.
 */
public class SharedPreferenceAdapter {

    private static SharedPreferenceAdapter instance;
    private Context context;
    private SharedPreferences sharedPreferences;

    public static SharedPreferenceAdapter with(Context context) {
        if (instance == null) {
            instance = new SharedPreferenceAdapter();
        }
        instance.init(context);
        return instance;
    }

    private void init(Context context) {
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCE, 0);
    }

    public void setToken(String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TOKEN, value);
        editor.apply();
    }

    public String getToken() {
        return sharedPreferences.getString(TOKEN, "");
    }

    public void setCourse(int value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(COURSE, value);
        editor.apply();
    }

    public int getCourse() {
        return sharedPreferences.getInt(COURSE, 0);
    }

    private final static String SHARED_PREFERENCE = R.class.getPackage().getName() + ".prefs";

    private final static String TOKEN = R.class.getPackage().getName() + ".prefs.token";
    private final static String COURSE = R.class.getPackage().getName() + ".prefs.course";

}
