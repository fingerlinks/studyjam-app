package org.fingerlinks.mobile.android.studyjam.http.response.presence;

import org.fingerlinks.mobile.android.studyjam.http.response.BaseResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.presence.bean.Presence;

import java.util.ArrayList;

/**
 * Created by raphaelbussa on 01/03/16.
 */
public class PresenceResponse extends BaseResponse {

    private ArrayList<Presence> users;

    public ArrayList<Presence> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<Presence> users) {
        this.users = users;
    }
}
