package org.fingerlinks.mobile.android.studyjam.http.response.course.bean;

import java.io.Serializable;

/**
 * Created by raphaelbussa on 02/03/16.
 */
public class Course implements Serializable {

    private int id;
    private String name;
    private String description;
    private long start_date;
    private long end_date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getStart_date() {
        return start_date;
    }

    public void setStart_date(long start_date) {
        this.start_date = start_date;
    }

    public long getEnd_date() {
        return end_date;
    }

    public void setEnd_date(long end_date) {
        this.end_date = end_date;
    }
}
