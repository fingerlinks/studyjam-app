package org.fingerlinks.mobile.android.studyjam.ui.fragment;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.koushikdutta.async.future.FutureCallback;

import org.fingerlinks.mobile.android.studyjam.R;
import org.fingerlinks.mobile.android.studyjam.http.HttpRequestClient;
import org.fingerlinks.mobile.android.studyjam.http.response.presence.PresenceResponse;
import org.fingerlinks.mobile.android.studyjam.ui.adapter.PresentAdapter;
import org.fingerlinks.mobile.android.utils.fragment.BaseFragment;

/**
 * Created by raphaelbussa on 04/03/16.
 */
public class PresenceFragment extends BaseFragment {

    private int id;
    private PresentAdapter adapter;
    private MaterialDialog progress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        id = getArguments().getInt("ID");
        adapter = new PresentAdapter(getActivity(), id);
        ObservableRecyclerView recyclerView = (ObservableRecyclerView) view.findViewById(R.id.list);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
        recyclerView.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.dp_4)));
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        getUsers();
    }

    private void getUsers() {
        progress = new MaterialDialog.Builder(getActivity())
                .content(R.string.loading)
                .progress(true, 0)
                .cancelable(false)
                .build();
        progress.show();
        HttpRequestClient.presence(getActivity(), id, new FutureCallback<PresenceResponse>() {
            @Override
            public void onCompleted(Exception e, PresenceResponse result) {
                progress.dismiss();
                if (e != null) {
                    return;
                }
                if (result.isSuccess()) {
                    adapter.setLessonModelList(result.getUsers());
                }
            }
        });
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {

        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int current = parent.getChildAdapterPosition(view);
            outRect.top = space + space;
            outRect.left = space + space;
            outRect.right = space + space;
            if (current == parent.getAdapter().getItemCount() - 1) {
                outRect.bottom = space + space;
            }
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_course;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) menu.clear();
        inflater.inflate(R.menu.presence_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.refresh:
                getUsers();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}

