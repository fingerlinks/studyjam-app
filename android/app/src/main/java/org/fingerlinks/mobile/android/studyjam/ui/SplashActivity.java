package org.fingerlinks.mobile.android.studyjam.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.koushikdutta.async.future.FutureCallback;

import org.fingerlinks.mobile.android.navigator.Navigator;
import org.fingerlinks.mobile.android.studyjam.R;
import org.fingerlinks.mobile.android.studyjam.factory.UserFactory;
import org.fingerlinks.mobile.android.studyjam.http.HttpRequestClient;
import org.fingerlinks.mobile.android.studyjam.http.bean.Login;
import org.fingerlinks.mobile.android.studyjam.http.response.login.LoginResponse;
import org.fingerlinks.mobile.android.utils.Utils;
import org.fingerlinks.mobile.android.utils.activity.BaseActivity;

public class SplashActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = SplashActivity.class.getName();
    private static final int RC_SIGN_IN = 9001;

    private GoogleApiClient googleApiClient;
    private RelativeLayout root;
    private SignInButton signInButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        root = (RelativeLayout) findViewById(R.id.root);

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();

        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setScopes(googleSignInOptions.getScopeArray());
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
        if (!UserFactory.with(SplashActivity.this).isLogged()) {
            signInButton.setVisibility(View.VISIBLE);
            /*YoYo.with(Techniques.SlideInUp)
                    //.delay(10)
                    .duration(R.integer.anim_speed)
                    .playOn(signInButton);*/
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount googleSignInAccount = result.getSignInAccount();

            if (googleSignInAccount == null) {
                onError();
                return;
            }

            Login login = new Login();
            login.setUsername(googleSignInAccount.getDisplayName());
            login.setEmail(googleSignInAccount.getEmail());
            login.setAvatar(Utils.removeNull(String.valueOf(googleSignInAccount.getPhotoUrl())));

            HttpRequestClient.login(SplashActivity.this, login, new FutureCallback<LoginResponse>() {
                @Override
                public void onCompleted(Exception e, LoginResponse result) {
                    if (e != null) {
                        onError();
                        return;
                    }
                    if (result.isSuccess()) {
                        Snackbar.make(root, Utils.encodeString(getString(R.string.success_login_msg, result.getUser().getName())), Snackbar.LENGTH_SHORT).show();
                        UserFactory.with(SplashActivity.this).login(result.getUser());
                        goToHomePage();
                    } else {
                        onError();
                    }
                }
            });
        }
    }

    private void onError() {

        Auth.GoogleSignInApi.signOut(googleApiClient);

        Snackbar.make(root, R.string.error_login_msg, Snackbar.LENGTH_LONG)
                .setAction(R.string.error_login_act, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                        startActivityForResult(signInIntent, RC_SIGN_IN);
                    }
                })
                .setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        super.onDismissed(snackbar, event);
                        signInButton.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onShown(Snackbar snackbar) {
                        super.onShown(snackbar);
                        signInButton.setVisibility(View.GONE);
                    }
                })
                .show();
    }

    private void goToHomePage() {
        //signInButton.setVisibility(View.GONE);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Navigator.with(SplashActivity.this)
                        .build()
                        .goTo(MainActivity.class)
                        .animation()
                        .commit();
                finish();
            }
        }, 500);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (UserFactory.with(SplashActivity.this).isLogged()) {
            OptionalPendingResult<GoogleSignInResult> optionalPendingResult = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
            if (optionalPendingResult.isDone()) {
                GoogleSignInResult result = optionalPendingResult.get();
                handleSignInResult(result);
            } else {
                optionalPendingResult.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                    @Override
                    public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                        handleSignInResult(googleSignInResult);
                    }
                });
            }
            //goToHomePage();
            return;
        }
        /*OptionalPendingResult<GoogleSignInResult> optionalPendingResult = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (optionalPendingResult.isDone()) {
            GoogleSignInResult result = optionalPendingResult.get();
            handleSignInResult(result);
        } else {
            optionalPendingResult.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    handleSignInResult(googleSignInResult);
                }
            });
        }*/
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        onError();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected int getToolbarId() {
        return 0;
    }

    @Override
    protected int getToolbarShadowId() {
        return 0;
    }

}
