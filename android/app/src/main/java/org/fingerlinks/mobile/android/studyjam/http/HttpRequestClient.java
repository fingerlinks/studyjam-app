package org.fingerlinks.mobile.android.studyjam.http;

import android.content.Context;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import org.fingerlinks.mobile.android.studyjam.Constants;
import org.fingerlinks.mobile.android.studyjam.SharedPreferenceAdapter;
import org.fingerlinks.mobile.android.studyjam.http.bean.Login;
import org.fingerlinks.mobile.android.studyjam.http.bean.UpdateUser;
import org.fingerlinks.mobile.android.studyjam.http.response.BaseResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.course.CourseResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.lesson.LessonResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.login.LoginResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.presence.PresenceResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.user.AttendanceResponse;

/**
 * Created by raphaelbussa on 01/03/16.
 */
public class HttpRequestClient {

    public static void login(Context context, Login login, FutureCallback<LoginResponse> futureCallback) {
        String url = Constants.BASE_URL + "/login";
        Ion.with(context)
                .load("POST", url)
                .addHeader("api-key", Constants.API_KEY)
                .setBodyParameter("username", login.getUsername())
                .setBodyParameter("avatar", login.getAvatar())
                .setBodyParameter("email", login.getEmail())
                .as(LoginResponse.class)
                .setCallback(futureCallback);
    }

    public static void user(Context context, int user_id, FutureCallback<LoginResponse> futureCallback) {
        String url = Constants.BASE_URL + "/user/" + user_id;
        String token = SharedPreferenceAdapter.with(context).getToken();
        Ion.with(context)
                .load("GET", url)
                .addHeader("api-key", Constants.API_KEY)
                .addHeader("token", token)
                .as(LoginResponse.class)
                .setCallback(futureCallback);
    }

    public static void self(Context context, FutureCallback<LoginResponse> futureCallback) {
        String url = Constants.BASE_URL + "/self";
        String token = SharedPreferenceAdapter.with(context).getToken();
        Ion.with(context)
                .load("GET", url)
                .addHeader("api-key", Constants.API_KEY)
                .addHeader("token", token)
                .as(LoginResponse.class)
                .setCallback(futureCallback);
    }

    public static void course(Context context, FutureCallback<CourseResponse> futureCallback) {
        String url = Constants.BASE_URL + "/course";
        String token = SharedPreferenceAdapter.with(context).getToken();
        Ion.with(context)
                .load("GET", url)
                .addHeader("api-key", Constants.API_KEY)
                .addHeader("token", token)
                .as(CourseResponse.class)
                .setCallback(futureCallback);
    }

    public static void lessons(Context context, int id_course, FutureCallback<LessonResponse> futureCallback) {
        String url = Constants.BASE_URL + "/lesson?id_course=" + id_course;
        String token = SharedPreferenceAdapter.with(context).getToken();
        Ion.with(context)
                .load("GET", url)
                .addHeader("api-key", Constants.API_KEY)
                .addHeader("token", token)
                .as(LessonResponse.class)
                .setCallback(futureCallback);
    }

    public static void attendance(Context context, FutureCallback<AttendanceResponse> futureCallback) {
        String url = Constants.BASE_URL + "/attendance";
        String token = SharedPreferenceAdapter.with(context).getToken();
        Ion.with(context)
                .load("GET", url)
                .addHeader("api-key", Constants.API_KEY)
                .addHeader("token", token)
                .as(AttendanceResponse.class)
                .setCallback(futureCallback);
    }

    public static void setAttendance(Context context, boolean present, int id_user, int id_lesson, FutureCallback<BaseResponse> futureCallback) {
        String url = Constants.BASE_URL + "/attendance";
        String token = SharedPreferenceAdapter.with(context).getToken();
        String course = String.valueOf(SharedPreferenceAdapter.with(context).getCourse());
        String value;
        if (present) {
            value = "1";
        } else {
            value = "0";
        }
        Ion.with(context)
                .load("POST", url)
                .addHeader("api-key", Constants.API_KEY)
                .addHeader("token", token)
                .setBodyParameter("id_course", course)
                .setBodyParameter("id_lesson", String.valueOf(id_lesson))
                .setBodyParameter("id_user", String.valueOf(id_user))
                .setBodyParameter("valid", value)
                .as(BaseResponse.class)
                .setCallback(futureCallback);
    }

    public static void presence(Context context, int id, FutureCallback<PresenceResponse> futureCallback) {
        String url = Constants.BASE_URL + "/lesson/users/" + id;
        String token = SharedPreferenceAdapter.with(context).getToken();
        Ion.with(context)
                .load("GET", url)
                .addHeader("api-key", Constants.API_KEY)
                .addHeader("token", token)
                .as(PresenceResponse.class)
                .setCallback(futureCallback);
    }

    public static void updateUser(Context context, UpdateUser updateUser, FutureCallback<LoginResponse> futureCallback) {
        String url = Constants.BASE_URL + "/user";
        String token = SharedPreferenceAdapter.with(context).getToken();
        Ion.with(context)
                .load("PATCH", url)
                .addHeader("api-key", Constants.API_KEY)
                .addHeader("token", token)
                .setBodyParameter("github", updateUser.getGithub())
                .setBodyParameter("telegram", updateUser.getTelegram())
                .setBodyParameter("whatsapp", updateUser.getWhatsapp())
                .as(LoginResponse.class)
                .setCallback(futureCallback);
    }

}
