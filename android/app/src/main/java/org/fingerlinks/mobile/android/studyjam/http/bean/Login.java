package org.fingerlinks.mobile.android.studyjam.http.bean;

import java.io.Serializable;

/**
 * Created by raphaelbussa on 01/03/16.
 */
public class Login implements Serializable {

    private String username;
    private String avatar;
    private String email;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
