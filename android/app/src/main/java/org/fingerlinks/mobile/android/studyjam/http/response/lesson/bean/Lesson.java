package org.fingerlinks.mobile.android.studyjam.http.response.lesson.bean;

import java.io.Serializable;

/**
 * Created by raphaelbussa on 02/03/16.
 */
public class Lesson implements Serializable {

    private int id;
    private int id_user;
    private int id_course;
    private String title;
    private String descprition_short;
    private String descprition_long;
    private String link_drive;
    private String link_udacity;
    private String address;
    private double lat;
    private double lng;
    private long start_date;
    private long end_date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_course() {
        return id_course;
    }

    public void setId_course(int id_course) {
        this.id_course = id_course;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescprition_short() {
        return descprition_short;
    }

    public void setDescprition_short(String descprition_short) {
        this.descprition_short = descprition_short;
    }

    public String getDescprition_long() {
        return descprition_long;
    }

    public void setDescprition_long(String descprition_long) {
        this.descprition_long = descprition_long;
    }

    public String getLink_drive() {
        return link_drive;
    }

    public void setLink_drive(String link_drive) {
        this.link_drive = link_drive;
    }

    public String getLink_udacity() {
        return link_udacity;
    }

    public void setLink_udacity(String link_udacity) {
        this.link_udacity = link_udacity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public long getStart_date() {
        return start_date;
    }

    public void setStart_date(long start_date) {
        this.start_date = start_date;
    }

    public long getEnd_date() {
        return end_date;
    }

    public void setEnd_date(long end_date) {
        this.end_date = end_date;
    }
}
