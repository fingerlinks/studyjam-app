package org.fingerlinks.mobile.android.studyjam.http.response.course;

import org.fingerlinks.mobile.android.studyjam.http.response.BaseResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.course.bean.Course;

import java.util.ArrayList;

/**
 * Created by raphaelbussa on 01/03/16.
 */
public class CourseResponse extends BaseResponse {

    private ArrayList<Course> course;

    public ArrayList<Course> getCourse() {
        return course;
    }

    public void setCourse(ArrayList<Course> course) {
        this.course = course;
    }
}
