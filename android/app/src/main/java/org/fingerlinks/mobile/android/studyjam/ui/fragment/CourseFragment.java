package org.fingerlinks.mobile.android.studyjam.ui.fragment;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.koushikdutta.async.future.FutureCallback;
import com.mikepenz.iconics.view.IconicsImageView;

import org.fingerlinks.mobile.android.navigator.Navigator;
import org.fingerlinks.mobile.android.studyjam.Constants;
import org.fingerlinks.mobile.android.studyjam.R;
import org.fingerlinks.mobile.android.studyjam.SharedPreferenceAdapter;
import org.fingerlinks.mobile.android.studyjam.factory.ConvertFacory;
import org.fingerlinks.mobile.android.studyjam.factory.UserFactory;
import org.fingerlinks.mobile.android.studyjam.http.HttpRequestClient;
import org.fingerlinks.mobile.android.studyjam.http.bean.UpdateUser;
import org.fingerlinks.mobile.android.studyjam.http.response.lesson.LessonResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.login.LoginResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.login.bean.Attendance;
import org.fingerlinks.mobile.android.studyjam.http.response.user.AttendanceResponse;
import org.fingerlinks.mobile.android.studyjam.model.CourseModel;
import org.fingerlinks.mobile.android.studyjam.model.LessonModel;
import org.fingerlinks.mobile.android.studyjam.model.UserModel;
import org.fingerlinks.mobile.android.studyjam.ui.adapter.LessionAdapter;
import org.fingerlinks.mobile.android.utils.SocialIntent;
import org.fingerlinks.mobile.android.utils.Utils;
import org.fingerlinks.mobile.android.utils.adapter.HeaderViewRecyclerAdapter;
import org.fingerlinks.mobile.android.utils.fragment.BaseFragment;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

/**
 * Created by raphaelbussa on 02/03/16.
 */
public class CourseFragment extends BaseFragment implements ObservableScrollViewCallbacks {

    private static final String TAG = CourseFragment.class.getName();

    private View header;
    private ProgressBar progress;
    private int currentCourse;
    private TextView textProgress;
    private View cover;
    private LessionAdapter adapter;
    private int baseColor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        baseColor = getActivity().getResources().getColor(R.color.primary);
        currentCourse = SharedPreferenceAdapter.with(getActivity()).getCourse();
        adapter = new LessionAdapter(getActivity());
        HeaderViewRecyclerAdapter headerViewRecyclerAdapter = new HeaderViewRecyclerAdapter(adapter);
        ObservableRecyclerView recyclerView = (ObservableRecyclerView) view.findViewById(R.id.list);
        recyclerView.setScrollViewCallbacks(this);
        header = getActivity().getLayoutInflater().inflate(R.layout.header_course, null);
        headerViewRecyclerAdapter.addHeaderView(header);
        GridLayoutManager manager = new GridLayoutManager(getActivity(), 1);
        recyclerView.addItemDecoration(new SpacesItemDecoration(getResources().getDimensionPixelSize(R.dimen.dp_4)));
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(headerViewRecyclerAdapter);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LessonModel lessonModel = (LessonModel) v.getTag();
                Bundle bundle = new Bundle();
                bundle.putInt("ID", lessonModel.getId());
                Navigator.with(getActivity())
                        .build()
                        .goTo(Fragment.instantiate(getActivity(), DetailLessonFragment.class.getName()), bundle, R.id.container)
                        .addToBackStack()
                        .animation()
                        .tag(Constants.FRAGMENT_LESSION)
                        .add()
                        .commit();
            }
        });
        setUpHeader();
        loadLessons();
    }

    private void loadLessons() {
        HttpRequestClient.lessons(getActivity(), currentCourse, new FutureCallback<LessonResponse>() {
            @Override
            public void onCompleted(Exception e, LessonResponse result) {
                if (e != null) {
                    updateAdapterFromRealm();
                    return;
                }
                if (result.isSuccess()) {
                    ConvertFacory.saveLesson(result.getLesson());
                    updateAdapterFromRealm();
                } else {
                    updateAdapterFromRealm();
                }
            }
        });
    }

    private void updateAdapterFromRealm() {
        Realm realm = Realm.getDefaultInstance();
        List<LessonModel> lessonModelList = realm.where(LessonModel.class).equalTo("id_course", currentCourse).findAllSorted("start_date");
        adapter.setLessonModelList(lessonModelList);
    }

    private void setUpHeader() {
        UserModel userModel = UserFactory.with(getActivity()).getUser();
        CircleImageView avatar = (CircleImageView) header.findViewById(R.id.avatar);
        TextView name = (TextView) header.findViewById(R.id.name);
        TextView email = (TextView) header.findViewById(R.id.email);
        progress = (ProgressBar) header.findViewById(R.id.progress);
        textProgress = (TextView) header.findViewById(R.id.text_progress);
        IconicsImageView whatsapp = (IconicsImageView) header.findViewById(R.id.whatsapp);
        whatsapp.setTag(userModel.getWhatsapp());
        whatsapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = (String) view.getTag();
                SocialIntent.whatsapp(getActivity(), value);
            }
        });

        IconicsImageView telegram = (IconicsImageView) header.findViewById(R.id.telegram);
        telegram.setTag(userModel.getTelegram());
        telegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = (String) view.getTag();
                SocialIntent.telegram(getActivity(), value);
            }
        });

        IconicsImageView github = (IconicsImageView) header.findViewById(R.id.github);
        github.setTag(userModel.getGithub());
        github.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = (String) view.getTag();
                SocialIntent.github(getActivity(), value);
            }
        });

        IconicsImageView hangouts = (IconicsImageView) header.findViewById(R.id.hangouts);
        hangouts.setTag(userModel.getEmail());
        hangouts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = (String) view.getTag();
                SocialIntent.hangout(getActivity(), value);
            }
        });

        cover = header.findViewById(R.id.cover);

        checkIfValueIsPresent(userModel.getGithub(), github);
        checkIfValueIsPresent(userModel.getWhatsapp(), whatsapp);
        checkIfValueIsPresent(userModel.getTelegram(), telegram);
        checkIfValueIsPresent(userModel.getEmail(), hangouts);

        HttpRequestClient.attendance(getActivity(), new FutureCallback<AttendanceResponse>() {
            @Override
            public void onCompleted(Exception e, AttendanceResponse result) {
                if (e != null) return;
                if (result.isSuccess()) {
                    for (Attendance attendance : result.getAttendance()) {
                        if (attendance.getId_course() == currentCourse) {
                            progress.setMax(attendance.getNum_lesson());
                            progress.setProgress(attendance.getNum_lesson_user());
                            textProgress.setText(getString(R.string.progress_label, String.valueOf(attendance.getNum_lesson_user()), String.valueOf(attendance.getNum_lesson())));
                        }
                    }
                }
            }
        });

        email.setText(userModel.getEmail());
        name.setText(Utils.encodeString(userModel.getName()));
        Glide.with(getActivity())
                .load(userModel.getAvatar())
                .asBitmap()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(avatar);
    }

    private void checkIfValueIsPresent(String value, IconicsImageView iconicsButton) {
        if (TextUtils.isEmpty(value)) {
            iconicsButton.setVisibility(View.GONE);
        } else {
            iconicsButton.setVisibility(View.VISIBLE);
        }
    }

    public class SpacesItemDecoration extends RecyclerView.ItemDecoration {

        private int space;

        public SpacesItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int current = parent.getChildAdapterPosition(view);
            if (current != 0) {
                outRect.top = space + space;
                outRect.left = space + space;
                outRect.right = space + space;
                if (current == parent.getAdapter().getItemCount() - 1) {
                    outRect.bottom = space + space;
                }
            }
        }
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int header_h = header.getHeight();
        float alpha = Math.min(1, (float) scrollY / header_h) * (float) 1.5;
        cover.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_course;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (menu != null) menu.clear();
        inflater.inflate(R.menu.main_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.profile:
                showProfileDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private EditText github;
    private EditText telegram;
    private EditText whatsapp;
    private MaterialDialog progressDialog;

    private void showProfileDialog() {
        MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                .title(R.string.profile)
                .customView(R.layout.dialog_change_profile, true)
                .positiveText(R.string.update)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        progressDialog = new MaterialDialog.Builder(getActivity())
                                .content(R.string.loading)
                                .progress(true, 0)
                                .cancelable(false)
                                .build();
                        progressDialog.show();
                        UpdateUser up = new UpdateUser();
                        up.setGithub(github.getText().toString());
                        up.setTelegram(telegram.getText().toString());
                        up.setWhatsapp(whatsapp.getText().toString());
                        HttpRequestClient.updateUser(getActivity(), up, new FutureCallback<LoginResponse>() {
                            @Override
                            public void onCompleted(Exception e, LoginResponse result) {
                                progressDialog.dismiss();
                                if (e != null) {
                                    return;
                                }
                                if (result.isSuccess()) {
                                    UserFactory.with(getActivity()).updateUser(result.getUser());
                                    setUpHeader();
                                }
                            }
                        });
                    }
                }).build();

        github = (EditText) dialog.getCustomView().findViewById(R.id.github);
        telegram = (EditText) dialog.getCustomView().findViewById(R.id.telegram);
        whatsapp = (EditText) dialog.getCustomView().findViewById(R.id.whatsapp);

        UserModel us = UserFactory.with(getActivity()).getUser();
        github.setText(us.getGithub());
        telegram.setText(us.getTelegram());
        whatsapp.setText(us.getWhatsapp());
        dialog.show();
    }

}