package org.fingerlinks.mobile.android.studyjam.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.mikepenz.iconics.view.IconicsTextView;

import org.fingerlinks.mobile.android.studyjam.Constants;
import org.fingerlinks.mobile.android.studyjam.R;
import org.fingerlinks.mobile.android.studyjam.SharedPreferenceAdapter;
import org.fingerlinks.mobile.android.studyjam.model.LessonModel;
import org.fingerlinks.mobile.android.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raphaelbussa on 02/03/16.
 */
public class LessionAdapter extends RecyclerView.Adapter<LessionAdapter.MainViewHolder> {

    private List<LessonModel> lessonModelList;
    private Context context;
    private View.OnClickListener onClickListener;

    public LessionAdapter(Context context) {
        this.context = context;
        this.lessonModelList = new ArrayList<>();
    }

    public void setLessonModelList(List<LessonModel> lessonModelList) {
        this.lessonModelList.clear();
        this.lessonModelList = lessonModelList;
        this.notifyDataSetChanged();
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_lession, parent, false);
        return new MainViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        LessonModel lessonModel = lessonModelList.get(position);
        GlideUrl glideUrl = new GlideUrl(Constants.BASE_URL + "/user/avatar/" + lessonModel.getId_user(),
                new LazyHeaders.Builder()
                        .addHeader("api-key", Constants.API_KEY)
                        .addHeader("token", SharedPreferenceAdapter.with(context).getToken())
                        .build());
        Glide.with(context)
                .load(glideUrl)
                .asBitmap()
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.tutor_avatar);
        holder.drive.setTag(lessonModel.getLink_drive());
        holder.drive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = (String) v.getTag();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                context.startActivity(intent);

            }
        });
        holder.address.setText(lessonModel.getAddress());
        holder.lession_title.setText(lessonModel.getTitle());
        holder.date.setText(Utils.formatData(lessonModel.getStart_date()));
        holder.lession_desc.setText(lessonModel.getDescprition_short());
        holder.row.setTag(lessonModel);
        if (onClickListener != null) holder.row.setOnClickListener(onClickListener);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public int getItemCount() {
        return lessonModelList.size();
    }

    public class MainViewHolder extends RecyclerView.ViewHolder {

        private CardView row;
        private ImageView tutor_avatar;
        private TextView lession_title;
        private TextView lession_desc;
        private TextView date;
        private IconicsTextView drive;
        private TextView address;

        public MainViewHolder(View itemView) {
            super(itemView);
            row = (CardView) itemView.findViewById(R.id.row);
            tutor_avatar = (ImageView) itemView.findViewById(R.id.tutor_avatar);
            lession_title = (TextView) itemView.findViewById(R.id.lession_title);
            lession_desc = (TextView) itemView.findViewById(R.id.lession_desc);
            date = (TextView) itemView.findViewById(R.id.date);
            address = (TextView) itemView.findViewById(R.id.address);
            drive = (IconicsTextView) itemView.findViewById(R.id.drive);
        }
    }
}

