package org.fingerlinks.mobile.android.studyjam.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.koushikdutta.async.future.FutureCallback;

import org.fingerlinks.mobile.android.navigator.Navigator;
import org.fingerlinks.mobile.android.studyjam.Constants;
import org.fingerlinks.mobile.android.studyjam.R;
import org.fingerlinks.mobile.android.studyjam.SharedPreferenceAdapter;
import org.fingerlinks.mobile.android.studyjam.factory.ConvertFacory;
import org.fingerlinks.mobile.android.studyjam.http.HttpRequestClient;
import org.fingerlinks.mobile.android.studyjam.http.response.course.CourseResponse;
import org.fingerlinks.mobile.android.studyjam.model.CourseModel;
import org.fingerlinks.mobile.android.studyjam.ui.fragment.CourseFragment;
import org.fingerlinks.mobile.android.utils.Utils;
import org.fingerlinks.mobile.android.utils.activity.BaseActivity;

import io.realm.Realm;

/**
 * Created by raphaelbussa on 02/03/16.
 */
public class MainActivity extends BaseActivity implements FragmentManager.OnBackStackChangedListener {

    private MaterialDialog progress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    public void onSupportResume() {
        super.onSupportResume();
        int currentCourse = SharedPreferenceAdapter.with(MainActivity.this).getCourse();
        if (currentCourse == 0) {
            progress = new MaterialDialog.Builder(this)
                    .content(R.string.loading)
                    .progress(true, 0)
                    .cancelable(false)
                    .build();
            progress.show();
            HttpRequestClient.course(MainActivity.this, new FutureCallback<CourseResponse>() {
                @Override
                public void onCompleted(Exception e, final CourseResponse result) {
                    if (e != null) {
                        courseError();
                        return;
                    }
                    if (result.isSuccess()) {
                        progress.dismiss();
                        ConvertFacory.saveCourse(result.getCourse());
                        if (result.getCourse().size() == 1) {
                            SharedPreferenceAdapter.with(MainActivity.this).setCourse(result.getCourse().get(0).getId());
                            commitCourseFragment();
                        } else {
                            String[] items = new String[result.getCourse().size()];
                            for (int i = 0; i < result.getCourse().size(); i++) {
                                items[i] = result.getCourse().get(i).getName();
                            }
                            new MaterialDialog.Builder(MainActivity.this)
                                    .title(R.string.choose_course)
                                    .items(items)
                                    .cancelable(false)
                                    .itemsCallbackSingleChoice(2, new MaterialDialog.ListCallbackSingleChoice() {
                                        @Override
                                        public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                            SharedPreferenceAdapter.with(MainActivity.this).setCourse(result.getCourse().get(which).getId());
                                            commitCourseFragment();
                                            return true;
                                        }
                                    })
                                    .positiveText(R.string.choose)
                                    .show();
                        }
                    } else {
                        courseError();
                    }
                }
            });
        } else {
            commitCourseFragment();
        }
    }

    private void commitCourseFragment() {
        setSubtitleLesson();
        Navigator.with(MainActivity.this)
                .build()
                .goTo(Fragment.instantiate(MainActivity.this, CourseFragment.class.getName()), R.id.container)
                .addToBackStack()
                .tag(Constants.FRAGMENT_COURSE)
                .add()
                .commit();
    }

    private void courseError() {
        progress.dismiss();
        Utils.genericError(MainActivity.this, new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (Navigator.with(MainActivity.this).utils().getActualTag().equals(Constants.FRAGMENT_COURSE)) {
            Navigator.with(MainActivity.this).utils().confirmExitWithMessage(R.string.exit);
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected int getToolbarId() {
        return R.id.toolbar;
    }

    @Override
    protected int getToolbarShadowId() {
        return 0;
    }

    @Override
    public void onBackStackChanged() {
        String tag = Navigator.with(MainActivity.this).utils().getActualTag();
        switch (tag) {
            case Constants.FRAGMENT_COURSE:
                setSubtitleLesson();
                setNavigationIcon(null);
                setNavigationOnClickListener(null);
                break;
        }
    }

    private void setSubtitleLesson() {
        int currentCourse = SharedPreferenceAdapter.with(MainActivity.this).getCourse();
        Realm realm = Realm.getDefaultInstance();
        CourseModel courseModel = realm.where(CourseModel.class).equalTo("id", currentCourse).findFirst();
        setSubtitle(courseModel.getName());
    }

}
