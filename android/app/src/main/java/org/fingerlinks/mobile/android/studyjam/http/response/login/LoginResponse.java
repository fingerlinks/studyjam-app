package org.fingerlinks.mobile.android.studyjam.http.response.login;

import org.fingerlinks.mobile.android.studyjam.http.response.BaseResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.login.bean.User;

/**
 * Created by raphaelbussa on 01/03/16.
 */
public class LoginResponse extends BaseResponse {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
