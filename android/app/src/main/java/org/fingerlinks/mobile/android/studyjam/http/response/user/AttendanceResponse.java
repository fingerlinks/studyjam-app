package org.fingerlinks.mobile.android.studyjam.http.response.user;

import org.fingerlinks.mobile.android.studyjam.http.response.BaseResponse;
import org.fingerlinks.mobile.android.studyjam.http.response.login.bean.Attendance;

import java.util.ArrayList;

/**
 * Created by raphaelbussa on 01/03/16.
 */
public class AttendanceResponse extends BaseResponse {

    private int id_user;
    private ArrayList<Attendance> attendance;

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public ArrayList<Attendance> getAttendance() {
        return attendance;
    }

    public void setAttendance(ArrayList<Attendance> attendance) {
        this.attendance = attendance;
    }
}
