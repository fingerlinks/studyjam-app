package org.fingerlinks.mobile.android.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

/**
 * Created by raphaelbussa on 04/03/16.
 */
public class SocialIntent {

    public static void github(Context context, String value) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/" + value));
        context.startActivity(intent);
    }

    public static void whatsapp(Context context, String value) {
        String packageName = "com.whatsapp";
        if (!isAppAvailable(context, packageName)) return;
        Uri uri = Uri.parse("smsto:" + value);
        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
        intent.setPackage(packageName);
        context.startActivity(intent);
    }

    public static void telegram(Context context, String value) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://telegram.me/" + value));
        context.startActivity(intent);
    }

    public static void hangout(Context context, String value) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/hangouts/_/CONVERSATION/" + value));
        context.startActivity(intent);
    }

    private static boolean isAppAvailable(Context context, String appName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

}
