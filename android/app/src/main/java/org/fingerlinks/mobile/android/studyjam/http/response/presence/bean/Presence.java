package org.fingerlinks.mobile.android.studyjam.http.response.presence.bean;

import java.io.Serializable;

/**
 * Created by raphaelbussa on 04/03/16.
 */
public class Presence implements Serializable {

    private boolean present;
    private int id;
    private String username;
    private String avatar;

    public boolean isPresent() {
        return present;
    }

    public void setPresent(boolean present) {
        this.present = present;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
